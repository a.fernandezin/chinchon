export interface Meeting {
  date?: string;
  description?: string;
  doc?: string;
  docRepresent?: string;
  docAdvert?: string;
  docExtension?: string;
  docSession?: string;
  tabSelected?:number;
  titleImport?:boolean;
  
   
}
