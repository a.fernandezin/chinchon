import { Component, OnInit, Input, ViewEncapsulation, HostBinding } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Meeting } from 'src/app/model/data';

@Component({
  selector: 'app-meeting',
  templateUrl: './meeting.component.html',
  styleUrls: ['./meeting.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MeetingComponent implements OnInit {

  @Input() meeting: Meeting;

  @HostBinding('class.title-import') titleImport:boolean ;  

  constructor() { }

  ngOnInit() {
    this.titleImport= this.meeting.titleImport;
  }

}
